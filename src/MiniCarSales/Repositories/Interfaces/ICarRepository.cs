﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MiniCarSales.Domain.Models;

namespace MiniCarSales.Repositories.Interfaces
{
    public interface ICarRepository
    {
        Task<Guid> AddCar(Car car);

        Task<Car> GetCar(Guid id);

        IEnumerable<Car> GetAllCars();

        Task UpdateCar(Car car);
    }
}