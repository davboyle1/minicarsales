﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using MiniCarSales.Domain.Models;
using MiniCarSales.Repositories.Interfaces;
using Newtonsoft.Json;

namespace MiniCarSales.Repositories
{
    public class CarRepository : ICarRepository
    {
        private readonly DocumentClient _documentClient;
        private readonly string _databaseName;
        private readonly string _collectionName;

        public CarRepository(DocumentClient documentClient)
        {
            _documentClient = documentClient;
            _databaseName = "Cars";
            _collectionName = "CarsCollection";
        }

        public async Task<Guid> AddCar(Car car)
        {
            try
            {
                Car existingCar = await GetCar(car.Id);
                throw new ArgumentException();
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    car.Id = Guid.NewGuid();
                    await _documentClient.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName), car);
                    return car.Id;
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<Car> GetCar(Guid id)
        {
            ResourceResponse<Document> doc = await _documentClient.ReadDocumentAsync(UriFactory.CreateDocumentUri(_databaseName, _collectionName, id.ToString()));
            return JsonConvert.DeserializeObject<Car>(doc.Resource.ToString());
        }

        public IEnumerable<Car> GetAllCars()
        {
            return _documentClient.CreateDocumentQuery<Car>(
                 UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName));
        }

        public async Task UpdateCar(Car car)
        {
            try
            {
                await this._documentClient.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(_databaseName, _collectionName, car.Id.ToString()), car);
            }
            catch (DocumentClientException de)
            {
                throw de;
            }
        }
    }
}