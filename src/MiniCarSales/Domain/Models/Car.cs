﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MiniCarSales.Domain.Models
{
    public class Car
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "photo")]
        public string Photo { get; set; }

        [JsonProperty(PropertyName = "make")]
        public string Make { get; set; }

        [JsonProperty(PropertyName = "model")]
        public string Model { get; set; }

        [JsonProperty(PropertyName = "year")]
        public int Year { get; set; }

        [JsonProperty(PropertyName = "price")]
        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "dealerEmail")]
        public string DealerEmail { get; set; }

        [JsonProperty(PropertyName = "dealerName")]
        public string DealerName { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "abn")]
        public string ABN { get; set; }

        [JsonProperty(PropertyName = "vehicleDescription")]
        public string VehicleDescription { get; set; }

        [JsonProperty(PropertyName = "enquiries")]
        public IList<Enquiry> Enquiries { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}