﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MiniCarSales.Domain.Models;
using MiniCarSales.Domain.Services.Interfaces;
using MiniCarSales.Repositories.Interfaces;

namespace MiniCarSales.Domain.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepositry)
        {
            _carRepository = carRepositry;
        }

        public async Task<Guid> AddCar(Car car)
        {
            return await _carRepository.AddCar(car);
        }

        public async Task<Car> GetCar(Guid id)
        {
            return await _carRepository.GetCar(id);
        }

        public IEnumerable<Car> GetAllCars()
        {
            return _carRepository.GetAllCars();
        }

        public async Task UpdateCar(Car car)
        {
            await _carRepository.UpdateCar(car);
        }
    }
}