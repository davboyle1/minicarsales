﻿import { Component } from "angular2/core";
import { CarService } from "./car.service";
import { HTTP_PROVIDERS } from "angular2/http";
import { ICar } from "./ICar";
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf, NgForm, Control, ControlGroup, FormBuilder, Validators} from 'angular2/common';
import {RouteParams} from 'angular2/router';
import { Observable } from "rxjs/observable";

@Component({
    selector: "viewcardetails",
    templateUrl: "app/viewCarDetailsPage.html",
    providers: [CarService, HTTP_PROVIDERS]
})

export class ViewCarDetailsComponent {
    viewCarDetailsForm: ControlGroup;
    dealer = true;
    carId: string;
    car;

    id: Control = new Control("", Validators.required);
    photo: Control = new Control("", Validators.required);
    make: Control = new Control("", Validators.required);
    model: Control = new Control("", Validators.required);
    year: Control = new Control("", Validators.required);
    price: Control = new Control("", Validators.required);
    dealerEmail: Control = new Control("", Validators.required);
    dealerName: Control = new Control("", Validators.required);
    phone: Control = new Control("", Validators.required);
    abn: Control = new Control("", Validators.required);
    vehicleDescription: Control = new Control("", Validators.required);

    constructor(private carService: CarService, public _formBuilder: FormBuilder, params: RouteParams) {
        this.carId = params.get("id");
        carService.getCar(this.carId)
            .subscribe((c: ICar) => {
                this.car = c
            });

        this.viewCarDetailsForm = this._formBuilder.group({
            id: this.id,
            photo: this.photo,
            make: this.make,
            model: this.model,
            year: this.year,
            price: this.price,
            dealerEmail: this.dealerEmail,
            dealerName: this.dealerName,
            phone: this.phone,
            abn: this.abn,
            vehicleDescription: this.vehicleDescription
        });

        setTimeout(() => {
            this.id.updateValue(this.car.id);
            this.photo.updateValue(this.car.photo);
            this.make.updateValue(this.car.make);
            this.model.updateValue(this.car.model);
            this.year.updateValue(this.car.year);
            this.price.updateValue(this.car.price);
            this.dealerEmail.updateValue(this.car.dealerEmail);
            this.dealerName.updateValue(this.car.dealerName);
            this.phone.updateValue(this.car.phone);
            this.abn.updateValue(this.car.abn);
            this.vehicleDescription.updateValue(this.car.vehicleDescription);
        }, 500);
    }

    onSubmit(value) {
        this.carService.putCars(value)
            .subscribe();
    }
}