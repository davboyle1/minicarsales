"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
///<reference path="../../node_modules/angular2/typings/browser.d.ts" />
var browser_1 = require("angular2/platform/browser");
var core_1 = require("angular2/core");
var router_1 = require("angular2/router");
var carlist_component_1 = require("./carlist.component");
var router_2 = require("angular2/router");
var router_3 = require("angular2/router");
var addCardetails_component_1 = require("./addCardetails.component");
var viewCardetails_component_1 = require("./viewCardetails.component");
var AppComponent = (function () {
    function AppComponent() {
        this.dealer = true;
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: "my-app",
            template: "<a [routerLink]=\"['CarList']\">Car List</a>\n                <a *ngIf=\"dealer\" [routerLink]=\"['AddCarDetails']\">Add Car</a>\n                <router-outlet ></router-outlet>",
            directives: [carlist_component_1.CarListComponent, router_2.ROUTER_DIRECTIVES, router_2.RouterOutlet, router_2.RouterLink]
        }),
        router_3.RouteConfig([
            {
                path: "/carlist",
                name: "CarList",
                component: carlist_component_1.CarListComponent,
                useAsDefault: true
            },
            {
                path: "/addcardetails",
                name: "AddCarDetails",
                component: addCardetails_component_1.AddCarDetailsComponent
            },
            {
                path: "/viewcardetails/:id",
                name: "ViewCarDetails",
                component: viewCardetails_component_1.ViewCarDetailsComponent
            }
        ]), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
browser_1.bootstrap(AppComponent, [router_1.ROUTER_PROVIDERS]);
//# sourceMappingURL=app.component.js.map