"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("angular2/core");
var car_service_1 = require("./car.service");
var http_1 = require("angular2/http");
var common_1 = require('angular2/common');
var router_1 = require('angular2/router');
var ViewCarDetailsComponent = (function () {
    function ViewCarDetailsComponent(carService, _formBuilder, params) {
        var _this = this;
        this.carService = carService;
        this._formBuilder = _formBuilder;
        this.dealer = true;
        this.id = new common_1.Control("", common_1.Validators.required);
        this.photo = new common_1.Control("", common_1.Validators.required);
        this.make = new common_1.Control("", common_1.Validators.required);
        this.model = new common_1.Control("", common_1.Validators.required);
        this.year = new common_1.Control("", common_1.Validators.required);
        this.price = new common_1.Control("", common_1.Validators.required);
        this.dealerEmail = new common_1.Control("", common_1.Validators.required);
        this.dealerName = new common_1.Control("", common_1.Validators.required);
        this.phone = new common_1.Control("", common_1.Validators.required);
        this.abn = new common_1.Control("", common_1.Validators.required);
        this.vehicleDescription = new common_1.Control("", common_1.Validators.required);
        this.carId = params.get("id");
        carService.getCar(this.carId)
            .subscribe(function (c) {
            _this.car = c;
        });
        this.viewCarDetailsForm = this._formBuilder.group({
            id: this.id,
            photo: this.photo,
            make: this.make,
            model: this.model,
            year: this.year,
            price: this.price,
            dealerEmail: this.dealerEmail,
            dealerName: this.dealerName,
            phone: this.phone,
            abn: this.abn,
            vehicleDescription: this.vehicleDescription
        });
        setTimeout(function () {
            _this.id.updateValue(_this.car.id);
            _this.photo.updateValue(_this.car.photo);
            _this.make.updateValue(_this.car.make);
            _this.model.updateValue(_this.car.model);
            _this.year.updateValue(_this.car.year);
            _this.price.updateValue(_this.car.price);
            _this.dealerEmail.updateValue(_this.car.dealerEmail);
            _this.dealerName.updateValue(_this.car.dealerName);
            _this.phone.updateValue(_this.car.phone);
            _this.abn.updateValue(_this.car.abn);
            _this.vehicleDescription.updateValue(_this.car.vehicleDescription);
        }, 500);
    }
    ViewCarDetailsComponent.prototype.onSubmit = function (value) {
        this.carService.putCars(value)
            .subscribe();
    };
    ViewCarDetailsComponent = __decorate([
        core_1.Component({
            selector: "viewcardetails",
            templateUrl: "app/viewCarDetailsPage.html",
            providers: [car_service_1.CarService, http_1.HTTP_PROVIDERS]
        }), 
        __metadata('design:paramtypes', [car_service_1.CarService, common_1.FormBuilder, router_1.RouteParams])
    ], ViewCarDetailsComponent);
    return ViewCarDetailsComponent;
}());
exports.ViewCarDetailsComponent = ViewCarDetailsComponent;
//# sourceMappingURL=viewCardetails.component.js.map