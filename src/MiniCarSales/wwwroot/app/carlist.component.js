"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("angular2/core");
var car_service_1 = require("./car.service");
var http_1 = require("angular2/http");
var router_1 = require("angular2/router");
var CarListComponent = (function () {
    function CarListComponent(carService, router) {
        var _this = this;
        this.router = router;
        this.site = "Mini Car Sales";
        this.dealer = true;
        carService.getCars()
            .subscribe(function (c) {
            _this.cars = c;
        });
    }
    CarListComponent.prototype.onSelect = function (car) {
        this.router.navigate(['ViewCarDetails', { id: car.id }]);
    };
    CarListComponent = __decorate([
        core_1.Component({
            selector: "carlist",
            templateUrl: "app/carListPage.html",
            providers: [car_service_1.CarService, http_1.HTTP_PROVIDERS, router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [car_service_1.CarService, router_1.Router])
    ], CarListComponent);
    return CarListComponent;
}());
exports.CarListComponent = CarListComponent;
//# sourceMappingURL=carlist.component.js.map