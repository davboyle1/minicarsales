﻿export interface IAddCarRequest {
    photo: string;
    make: string;
    model: string;
    year: number;
    price: number;
    dealerEmail: string;
    dealerName: string;
    phone: string;
    abn: string;
    vehicleDescription: string;
}