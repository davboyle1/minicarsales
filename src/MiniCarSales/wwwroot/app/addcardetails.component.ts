﻿import { Component, OnInit } from "angular2/core";
import { CarService } from "./car.service";
import { HTTP_PROVIDERS } from "angular2/http";
import { IAddCarRequest } from "./IAddCarRequest";
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgIf, NgForm, Control, ControlGroup, FormBuilder, Validators} from 'angular2/common';

@Component({
    selector: "addcardetails",
    templateUrl: "app/addCarDetailsPage.html",
    providers: [CarService, HTTP_PROVIDERS]
})

export class AddCarDetailsComponent implements OnInit {
    addCarDetailsForm: ControlGroup;
    dealer = true;
    constructor(private carService: CarService, private _formBuilder: FormBuilder) {
    }

    onSubmit(value) {
        this.carService.postCars(value)
            .subscribe();
    }

    ngOnInit() {
        this.addCarDetailsForm = this._formBuilder.group({
            "photo": ["", Validators.required],
            "make": ["", Validators.required],
            "model": ["", Validators.required],
            "year": ["", Validators.required],
            "price": ["", Validators.required],
            "dealerEmail": ["", Validators.required],
            "dealerName": ["", Validators.required],
            "phone": ["", Validators.required],
            "abn": ["", Validators.required],
            "vehicleDescription": ["", Validators.required],
        });
    }
}