﻿import { Component } from "angular2/core";
import { CarService } from "./car.service";
import { HTTP_PROVIDERS } from "angular2/http";
import { ICar } from "./ICar";
import { ROUTER_DIRECTIVES, RouteConfig, Router } from "angular2/router";
import { ViewCarDetailsComponent } from "./viewCardetails.component";

@Component({
    selector: "carlist",
    templateUrl: "app/carListPage.html",
    providers: [CarService, HTTP_PROVIDERS, ROUTER_DIRECTIVES]
})

export class CarListComponent {
    site: string = "Mini Car Sales";
    private cars: Array<ICar>;

    dealer = true;

    constructor(carService: CarService, private router: Router) {
        carService.getCars()
            .subscribe(c => {
                this.cars = c;
            });
    }

    onSelect(car: ICar) {
        this.router.navigate(['ViewCarDetails', { id: car.id }]);
    }
}