﻿import { Injectable } from "angular2/core";
import { Http, HTTP_PROVIDERS, Headers } from "angular2/http";
import { Observable } from "rxjs/observable";
import "rxjs/Rx";
import "rxjs/add/operator/map";
import { ICar } from "./ICar";
import { IAddCarRequest } from "./IAddCarRequest";

@Injectable()
export class CarService {
    private cars;
    //private url = "http://minicarsales.azurewebsites.net/api/cars";
    private url = "http://localhost:63634/api/cars";

    constructor(private _http: Http) {
    }

    getCars(): Observable<Array<ICar>> {
        return this._http.get(this.url)
            .map(res => res.json());
    }

    getCar(id: string): Observable<ICar> {
        return this._http.get(this.url + "/" + id)
            .map(res => res.json());
    }

    postCars(car: IAddCarRequest) {
        var json = JSON.stringify(car);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.post(this.url, json, { headers: headers });
    }

    putCars(car: ICar) {
        var json = JSON.stringify(car);
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.put(this.url, json, { headers: headers });
    }
}