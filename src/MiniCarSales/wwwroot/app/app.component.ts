﻿///<reference path="../../node_modules/angular2/typings/browser.d.ts" />
import { bootstrap } from "angular2/platform/browser";
import { Component } from "angular2/core";
import { ROUTER_PROVIDERS } from "angular2/router";
import { CarListComponent } from "./carlist.component";
import { ROUTER_DIRECTIVES, RouterOutlet, RouterLink } from "angular2/router";
import { RouteConfig } from "angular2/router";
import { AddCarDetailsComponent } from "./addCardetails.component";
import { ViewCarDetailsComponent } from "./viewCardetails.component";

@Component({
    selector: "my-app",
    template: `<a [routerLink]="['CarList']">Car List</a>
                <a *ngIf="dealer" [routerLink]="['AddCarDetails']">Add Car</a>
                <router-outlet ></router-outlet>`,
    directives: [CarListComponent, ROUTER_DIRECTIVES, RouterOutlet, RouterLink]
})

@RouteConfig([
    {
        path: "/carlist",
        name: "CarList",
        component: CarListComponent,
        useAsDefault: true
    },
    {
        path: "/addcardetails",
        name: "AddCarDetails",
        component: AddCarDetailsComponent
    },
    {
        path: "/viewcardetails/:id",
        name: "ViewCarDetails",
        component: ViewCarDetailsComponent
    }

])
export class AppComponent {
    dealer = true;
}

bootstrap(AppComponent, [ROUTER_PROVIDERS]);