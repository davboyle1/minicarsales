﻿using System.ComponentModel.DataAnnotations;

namespace MiniCarSales.Models.CarModels
{
    public class AddCarRequest
    {
        [Required]
        public string Photo { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public string DealerEmail { get; set; }

        [Required]
        public string DealerName { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string ABN { get; set; }

        [Required]
        public string VehicleDescription { get; set; }
    }
}