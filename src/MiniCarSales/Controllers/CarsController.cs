using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MiniCarSales.Domain.Models;
using MiniCarSales.Domain.Services.Interfaces;
using MiniCarSales.Models.CarModels;

namespace MiniCarSales.Controllers
{
    [Produces("application/json")]
    [Route("api/Cars")]
    public class CarsController : Controller
    {
        private readonly ICarService _carService;

        public CarsController(ICarService carService)
        {
            _carService = carService;
        }

        // GET: api/Cars
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_carService.GetAllCars());
        }

        // GET: api/Cars/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(Guid id)
        {
            Car car;
            try
            {
                car = await _carService.GetCar(id);
                if (car == null)
                {
                    return NotFound();
                }
            }
            catch
            {
                return NotFound();
            }

            return Ok(car);
        }

        // POST: api/Cars
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddCarRequest carRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Car car = ConvertCar(carRequest);
                Guid carId = await _carService.AddCar(car);
                return CreatedAtRoute("Get", new { controller = "Cars", id = carId }, null);
            }
            catch
            {
                return BadRequest();
            }
        }

        private Car ConvertCar(AddCarRequest carRequest)
        {
            return new Car
            {
                Photo = carRequest.Photo,
                Make = carRequest.Make,
                Model = carRequest.Model,
                Year = carRequest.Year,
                Price = carRequest.Price,
                DealerEmail = carRequest.DealerEmail,
                DealerName = carRequest.DealerName,
                Phone = carRequest.Phone,
                ABN = carRequest.ABN,
                VehicleDescription = carRequest.VehicleDescription
            };
        }

        // PUT: api/Cars
        [HttpPut()]
        public async Task<IActionResult> Put([FromBody]UpdateCarRequest updateCarRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Car car = ConvertCar(updateCarRequest);
                await _carService.UpdateCar(car);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        private Car ConvertCar(UpdateCarRequest carRequest)
        {
            return new Car
            {
                Id = carRequest.Id,
                Photo = carRequest.Photo,
                Make = carRequest.Make,
                Model = carRequest.Model,
                Year = carRequest.Year,
                Price = carRequest.Price,
                DealerEmail = carRequest.DealerEmail,
                DealerName = carRequest.DealerName,
                Phone = carRequest.Phone,
                ABN = carRequest.ABN,
                VehicleDescription = carRequest.VehicleDescription
            };
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}